CSRC = $(shell find -name '*.c')
ASRC = $(shell find -name '*.S')
OBJS = $(patsubst %.c,%.o,$(CSRC)) $(patsubst %.S,%.o,$(ASRC))
DEPS = $(patsubst %.c,%.d,$(CSRC)) $(patsubst %.S,%.d,$(ASRC))

CC = gcc
LD = ld

ASFLAGS = -m32
CFLAGS = -MD -m32 -Wall -ffreestanding -g -fno-stack-protector -I include -I libogg-1.3.2/include -I Tremor -nostdinc -std=gnu99 -mno-mmx -mno-sse -mno-sse2
LDFLAGS = -melf_i386 -Tkernel.ld $(shell gcc -m32 -print-libgcc-file-name)

kernel: $(OBJS)
	$(LD) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

%.o: %.S
	$(CC) $(ASFLAGS) -c -o $@ $<

-include $(DEPS)

clean:
	rm -f kernel $(OBJS) $(DEPS)

# null,null,null works around a qemu bug introduced in 5eba5a6632
# (the bootloader name "qemu" overwrites the modules' command lines)
run: kernel null
	qemu-system-i386 -machine accel=kvm:tcg -kernel kernel -initrd null,null,null,music.ogg,win.ogg,loss.ogg -soundhw ac97 -vga std

null:
	touch $@

.PHONY: clean run
