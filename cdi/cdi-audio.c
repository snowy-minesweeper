#include <audio.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <cdi/audio.h>
#include <cdi/lists.h>
#include <cdi/mem.h>


#define BUFFER_COUNT 8
#define INJECT_OFFSET 256 /* arbitrary */


static struct cdi_audio_device *audio_device;
static struct cdi_audio_driver *audio_driver;
static struct cdi_audio_stream *audio_stream;

static void (*buffer_cb)(int16_t *data, size_t samples);
static int current_device_buffer;

static struct cdi_mem_area *buffers[BUFFER_COUNT];
static int prebuffered_buffers;


void register_cdi_audio_device(struct cdi_audio_device *dev)
{
    if (audio_device || dev->record) {
        return;
    }

    audio_device = dev;
    audio_driver = (struct cdi_audio_driver *)dev->dev.driver;
    audio_stream = cdi_list_get(audio_device->streams, 0);

    if (audio_stream->sample_format != CDI_AUDIO_16SI) {
        audio_device = NULL;
        return;
    }

    audio_driver->set_sample_rate(audio_stream, 44100);
    audio_driver->set_number_of_channels(audio_device, 2);
    audio_driver->set_volume(audio_stream, 0xff);
}

bool init_audio(void (*callback)(int16_t *data, size_t samples))
{
    if (!audio_device) {
        return false;
    }

    for (int i = 0; i < BUFFER_COUNT; i++) {
        buffers[i] = cdi_mem_alloc(audio_stream->buffer_size * 2, CDI_MEM_VIRT_ONLY);
    }

    buffer_cb = callback;

    return true;
}

static void fetch_buffers(void)
{
    int hw_index = (current_device_buffer + prebuffered_buffers) % audio_stream->num_buffers;

    for (; prebuffered_buffers < BUFFER_COUNT; prebuffered_buffers++) {
        buffer_cb(buffers[prebuffered_buffers]->vaddr, audio_stream->buffer_size);
        audio_driver->transfer_data(audio_stream, hw_index, buffers[prebuffered_buffers], 0);

        hw_index = (hw_index + 1) % audio_stream->num_buffers;
    }
}

void audio_play(bool play)
{
    if (!audio_driver) {
        return;
    }

    audio_driver->change_device_status(audio_device, play ? CDI_AUDIO_PLAY : CDI_AUDIO_STOP);

    if (play) {
        fetch_buffers();
    }
}

void audio_mute(bool mute)
{
    audio_driver->set_volume(audio_stream, mute ? 0x00 : 0xff);
}

void inject_samples(int16_t *data, size_t count)
{
    cdi_audio_position_t pos;
    audio_driver->get_position(audio_stream, &pos);

    size_t position = pos.frame * 2 + INJECT_OFFSET;
    int buffer = position / audio_stream->buffer_size;
    int offset = position % audio_stream->buffer_size;

    while (count && buffer < prebuffered_buffers) {
        /* I need variables like this all the time but I still don't know a good
         * way to name them... */
        size_t this_buffer_count = audio_stream->buffer_size - offset;
        if (this_buffer_count > count) {
            this_buffer_count = count;
        }

        int16_t *out = buffers[buffer]->vaddr;
        out += offset;
        for (size_t i = 0; i < this_buffer_count; i++) {
            int val = out[i] + data[i];
            if (val < -0x8000) {
                val = -0x8000;
            } else if (val > 0x7fff) {
                val = 0x7fff;
            }
            out[i] = val;
        }
        data += this_buffer_count;

        offset = 0;
        buffer++;
        count -= this_buffer_count;
    }

    int hw_index = current_device_buffer;
    for (int i = 0; i < buffer; i++) {
        audio_driver->transfer_data(audio_stream, hw_index, buffers[i], 0);
        hw_index = (hw_index + 1) % audio_stream->num_buffers;
    }
}

void cdi_audio_buffer_completed(struct cdi_audio_stream *stream, size_t buffer)
{
    (void)stream;

    buffer = (buffer + 1) % audio_stream->num_buffers;

    cdi_audio_position_t pos;
    audio_driver->get_position(audio_stream, &pos);

    while (current_device_buffer != buffer) {
        struct cdi_mem_area *popped = buffers[0];
        memmove(&buffers[0], &buffers[1], sizeof(buffers[0]) * (BUFFER_COUNT - 1));
        buffers[BUFFER_COUNT - 1] = popped;

        current_device_buffer = (current_device_buffer + 1) % audio_stream->num_buffers;

        if (prebuffered_buffers) {
            prebuffered_buffers--;
        }
    }

    fetch_buffers();
}
