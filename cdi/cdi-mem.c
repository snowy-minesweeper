#include <stddef.h>
#include <stdlib.h>
#include <cdi/mem.h>

struct cdi_mem_area *cdi_mem_alloc(size_t size, cdi_mem_flags_t flags)
{
    if (flags & CDI_MEM_DMA_16M) {
        return NULL;
    }

    uint32_t alignment = 1 << (flags & CDI_MEM_ALIGN_MASK);

    struct cdi_mem_area *area = malloc(sizeof(*area));
    area->size = size;
    area->vaddr = (void *)(((uintptr_t)malloc(size + alignment - 1) + alignment - 1) & ~(alignment - 1));
    area->paddr.num = 1;
    area->paddr.items = malloc(sizeof(*area->paddr.items));
    area->paddr.items[0].start = (uintptr_t)area->vaddr;
    area->paddr.items[0].size = size;

    return area;
}
