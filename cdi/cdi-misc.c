#include <stdbool.h>
#include <stdint.h>
#include <cdi.h>
#include <cdi/misc.h>

extern volatile uint64_t elapsed_milliseconds;

static bool irq_occured[16];
static void (*irq_handlers[16])(struct cdi_device *);
static struct cdi_device *irq_devices[16];

void cdi_try_handle_irq(int irq)
{
    irq_occured[irq] = true;

    if (irq_handlers[irq]) {
        irq_handlers[irq](irq_devices[irq]);
    }
}

void cdi_register_irq(uint8_t irq, void (*handler)(struct cdi_device *), struct cdi_device *device)
{
    irq_handlers[irq] = handler;
    irq_devices[irq] = device;
}

int cdi_wait_irq(uint8_t irq, uint32_t timeout)
{
    uint64_t ems_start = elapsed_milliseconds;

    while (!irq_occured[irq] && timeout > elapsed_milliseconds - ems_start) {
        __asm__ __volatile__ ("hlt");
    }

    return irq_occured[irq] ? 0 : -1;
}

int cdi_reset_wait_irq(uint8_t irq)
{
    irq_occured[irq] = false;
    return 0;
}

int cdi_ioports_alloc(uint16_t start, uint16_t count)
{
    (void)start;
    (void)count;

    return 0;
}

int cdi_ioports_free(uint16_t start, uint16_t count)
{
    (void)start;
    (void)count;

    return 0;
}

void cdi_sleep_ms(uint32_t ms)
{
    uint64_t ems_start = elapsed_milliseconds;

    while (ms > elapsed_milliseconds - ems_start) {
        __asm__ __volatile__ ("hlt");
    }
}
