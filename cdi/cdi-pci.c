#include <io.h>
#include <stdlib.h>
#include <cdi/lists.h>
#include <cdi/pci.h>

#define __pci_inX(w, off_mask) \
    uint##w##_t pci_in##w(int bus, int device, int function, int offset) \
    { \
        uint##w##_t ret; \
        out32(0xcf8, 0x80000000 | ((bus & 0xff) << 16) | ((device & 0x1f) << 11) | ((function & 0x07) << 8) | (offset & 0xfc)); \
        ret = in##w(0xcfc + (offset & off_mask)); \
        return ret; \
    }

__pci_inX( 8, 3);
__pci_inX(16, 2);
__pci_inX(32, 0);


#define __pci_outX(w, off_mask) \
    void pci_out##w(int bus, int device, int function, int offset, uint##w##_t value) \
    { \
        out32(0xcf8, 0x80000000 | ((bus & 0xff) << 16) | ((device & 0x1f) << 11) | ((function & 0x07) << 8) | (offset & 0xfc)); \
        out##w(0xcfc + (offset & off_mask), value); \
    }

__pci_outX( 8, 3);
__pci_outX(16, 2);
__pci_outX(32, 0);


void cdi_pci_alloc_ioports(struct cdi_pci_device *device)
{
    (void)device;
}

void cdi_pci_free_ioports(struct cdi_pci_device *device)
{
    (void)device;
}

uint16_t cdi_pci_config_readw(struct cdi_pci_device *device, uint8_t offset)
{
    return pci_in16(device->bus, device->dev, device->function, offset);
}

void cdi_pci_config_writew(struct cdi_pci_device *device, uint8_t offset, uint16_t val)
{
    pci_out16(device->bus, device->dev, device->function, offset, val);
}
