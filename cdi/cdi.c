#include <stdlib.h>
#include <cdi.h>
#include <cdi/audio.h>
#include <cdi/lists.h>
#include <cdi/pci.h>

extern struct cdi_driver *__start_cdi_drivers, *__stop_cdi_drivers;

uint8_t pci_in8(int bus, int device, int function, int offset);
uint16_t pci_in16(int bus, int device, int function, int offset);
uint32_t pci_in32(int bus, int device, int function, int offset);

void pci_out8(int bus, int device, int function, int offset, uint8_t value);
void pci_out16(int bus, int device, int function, int offset, uint16_t value);
void pci_out32(int bus, int device, int function, int offset, uint32_t value);

extern void register_cdi_audio_device(struct cdi_audio_device *dev);

void cdi_driver_init(struct cdi_driver *driver)
{
    driver->devices = cdi_list_create();
}

void cdi_driver_destroy(struct cdi_driver *driver)
{
    struct cdi_device *dev;

    while ((dev = cdi_list_pop(driver->devices))) {
        if (driver->remove_device) {
            driver->remove_device(dev);
        }
        free(dev);
    }

    cdi_list_destroy(driver->devices);
}

void cdi_init(void)
{
}

static void add_pci_function(int bus, int device, int function)
{
    struct cdi_pci_device dev = {
        .bus_data.bus_type  = CDI_PCI,
        .bus                = bus,
        .dev                = device,
        .function           = function,
        .vendor_id          = pci_in16(bus, device, function, 0x00),
        .device_id          = pci_in16(bus, device, function, 0x02),
        .class_id           = pci_in8(bus, device, function, 0x0b),
        .subclass_id        = pci_in8(bus, device, function, 0x0a),
        .interface_id       = pci_in8(bus, device, function, 0x09),
        .rev_id             = pci_in8(bus, device, function, 0x08),
        .irq                = pci_in8(bus, device, function, 0x3c),
        .resources          = cdi_list_create()
    };

    for (int i = 0; i < 6; i++) {
        uint32_t bar = pci_in32(bus, device, function, 0x10 + i * 4);
        if (!bar) {
            continue;
        }

        struct cdi_pci_resource *res = malloc(sizeof(*res));

        if (bar & 1) {
            res->type = CDI_PCI_IOPORTS;
            res->start = bar & ~0x3;
            res->index = i;
            res->address = NULL;

            pci_out32(bus, device, function, 0x10 + i * 4, 0xfffffffc);
            res->length = ~(pci_in32(bus, device, function, 0x10 + i * 4) & 0xfffffffc) + 0x1;
            pci_out32(bus, device, function, 0x10 + i * 4, bar & ~0x2);
        } else if (((bar >> 1) & 0x3) <= 0x1) {
            res->type = CDI_PCI_MEMORY;
            res->start = bar & ~0xf;
            res->index = i;
            res->address = NULL;

            pci_out32(bus, device, function, 0x10 + i * 4, 0xfffffff0);
            res->length = ~(pci_in32(bus, device, function, 0x10 + i * 4) & 0xfffffff0) + 0x1;
            pci_out32(bus, device, function, 0x10 + i * 4, bar);

            if (!res->length) {
                free(res);
                continue;
            }
        }

        cdi_list_push(dev.resources, res);
    }

    struct cdi_driver **drvp = &__start_cdi_drivers;
    while (drvp < &__stop_cdi_drivers) {
        struct cdi_driver *drv = *drvp;

        if (drv->bus == CDI_PCI && drv->init_device) {
            struct cdi_device *cdi_dev = drv->init_device(&dev.bus_data);
            if (cdi_dev) {
                cdi_dev->driver = drv;
                cdi_list_push(drv->devices, cdi_dev);

                if (drv->type == CDI_AUDIO) {
                    register_cdi_audio_device((struct cdi_audio_device *)cdi_dev);
                }

                break;
            }
        }

        drvp++;
    }
}

static void enumerate_pci_bus(int bus)
{
    for (int dev = 0; dev < 32; dev++) {
        for (int fnc = 0; fnc < 8; fnc++) {
            if (pci_in16(bus, dev, fnc, 0x00) == 0xffff) {
                if (fnc) {
                    continue;
                } else {
                    break;
                }
            }

            switch (pci_in8(bus, dev, fnc, 0x0e) & 0x7f) {
                case 0:
                    add_pci_function(bus, dev, fnc);
                    break;

                case 1:
                    enumerate_pci_bus(pci_in8(bus, dev, fnc, 0x19));
            }

            if (!fnc && !(pci_in8(bus, dev, fnc, 0x0e) & 0x80)) {
                break;
            }
        }
    }
}

void load_cdi_drivers(void)
{
    struct cdi_driver **drvp = &__start_cdi_drivers;
    while (drvp < &__stop_cdi_drivers) {
        struct cdi_driver *drv = *drvp;

        if (drv->init != NULL) {
            drv->init();
        }

        if (drv->type == CDI_AUDIO) {
            for (size_t i = 0; i < cdi_list_size(drv->devices); i++) {
                register_cdi_audio_device((struct cdi_audio_device *)cdi_list_get(drv->devices, i));
            }
        }

        drvp++;
    }

    enumerate_pci_bus(0);
}
