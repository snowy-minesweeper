#include <stdarg.h>
#include "io.h"
#include "console.h"

static int x = 0;
static int y = 0;
static unsigned char cur_attr = 0x07;

char* video = (char*) 0xb8000;

static int kprintf_res = 0;

void console_init(void)
{
    /* Disable blinking cursor */
    int value;

    outb(0x3d4, 10);
    value = inb(0x3d5);
    value &= ~0x40;
    value |= 0x20;
    outb(0x3d5, value);

}

void console_set_cursor(int new_x, int new_y)
{
    x = new_x;
    y = new_y;
}

unsigned char console_set_attr(unsigned char attr)
{
    unsigned char old = cur_attr;
    cur_attr = attr;
    return old;
}

char console_get(int x, int y)
{
    return video[2 * (80 * y + x)];
}

char console_getc(int x, int y, unsigned char *attr)
{
    *attr = video[2 * (80 * y + x) + 1];
    return console_get(x, y);
}

void console_setc(int x, int y, char c, unsigned char attr)
{
    video[2 * (80 * y + x)] = c;
    video[2 * (80 * y + x) + 1] = attr;
}

void console_set(int x, int y, char c)
{
    console_setc(x, y, c, cur_attr);
}

void kputc(char c)
{
    if ((c == '\n') || (x > 79)) {
        x = 0;
        y++;
    }

    //outb(0x3f8, c);

    if (c == '\n') {
        return;
    }

    if (y > 24) {
        x = 0;
        y = 0;
    }

    video[2 * (y * 80 + x)] = c;
    video[2 * (y * 80 + x) + 1] = cur_attr;

    x++;
    kprintf_res++;
}

void kputs(const char* s)
{
    while (*s) {
        kputc(*s++);
    }
}

static void kputn(unsigned long x, int base)
{
    char buf[65];
    const char* digits = "0123456789abcdefghijklmnopqrstuvwxyz";
    char* p;

    if (base > 36) {
        return;
    }

    p = buf + 64;
    *p = '\0';
    do {
        *--p = digits[x % base];
        x /= base;
    } while (x);
    kputs(p);
}

void clrscr(void)
{
    int i;
    for (i = 0; i < 2 * 25 * 80; i++) {
        video[i] = 0;
    }

    x = y = 0;
}

int kprintf(const char* fmt, ...)
{
    va_list ap;
    const char* s;
    unsigned long n;

    va_start(ap, fmt);
    kprintf_res = 0;
    while (*fmt) {
        if (*fmt == '%') {
            fmt++;
            switch (*fmt) {
                case 's':
                    s = va_arg(ap, char*);
                    kputs(s);
                    break;
                case 'c':
                    n = va_arg(ap, int);
                    kputc(n);
                    break;
                case 'd':
                case 'u':
                    n = va_arg(ap, unsigned long int);
                    kputn(n, 10);
                    break;
                case 'x':
                case 'p':
                    n = va_arg(ap, unsigned long int);
                    kputn(n, 16);
                    break;
                case '%':
                    kputc('%');
                    break;
                case '\0':
                    goto out;
                default:
                    kputc('%');
                    kputc(*fmt);
                    break;
            }
        } else {
            kputc(*fmt);
        }

        fmt++;
    }

out:
    va_end(ap);

    return kprintf_res;
}
