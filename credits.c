#include "mm.h"
#include "console.h"
#include "rand.h"
#include "string.h"

/* QEMU contributors from 1 Jan - 17 December 2014 */
static const char *credits[] = {
    "Aakriti Gupta",
    "Adam Crume",
    "Adam Lackorzynski",
    "Adelina Tuvenie",
    "Adrian-Ken Rueegsegger",
    "Alex Barcelo",
    "Alex Benn\202e",
    "Alex Bligh",
    "Alex Williamson",
    "Alex Zuepke",
    "Alexander Graf",
    "Alexey Kardashevskiy",
    "Alistair Francis",
    "Alon Levy",
    "Amit Shah",
    "Amos Kong",
    "Andreas F\x84rber",
    "Andreas Schwab",
    "Andrei Warkentin",
    "Andrew Jones",
    "Andrew Oates",
    "Andr\x82 Hentschel",
    "Aneesh Kumar K.V",
    "Anthony Liguori",
    "Anthony PERARD",
    "Anton Blanchard",
    "Anton Ivanov",
    "Antony Pavlov",
    "Ard Biesheuvel",
    "Artyom Tarasenko",
    "Aurelien Jarno",
    "Avik Sil",
    "BALATON Zoltan",
    "Badari Pulavarty",
    "Bandan Das",
    "Bastian Blank",
    "Bastian Koppelmann",
    "Ben Draper",
    "Beniamino Galvani",
    "Benjamin Herrenschmidt",
    "Beno\x8ct Canet",
    "Bernhard \232belacker",
    "Bharat Bhushan",
    "Bharata B Rao",
    "Bin Wu",
    "Brad",
    "Bruce Rogers",
    "Chao Peng",
    "Chen Fan",
    "Chen Gang",
    "ChenLiang",
    "Chris Johns",
    "Chris Spiegel",
    "Christian Borntraeger",
    "Christian Burger",
    "Christoffer Dall",
    "Christoph Seifert",
    "Christophe Fergeau",
    "Christophe Lyon",
    "Chrysostomos Nanakos",
    "Chunyan Liu",
    "Claudio Fontana",
    "Cole Robinson",
    "Colin Leitner",
    "Corey Bryant",
    "Corey Minyard",
    "Cornelia Huck",
    "Craig Heffner",
    "C\202dric Le Goater",
    "Damjan Marion",
    "Daniel Henrique Barboza",
    "Daniel P. Berrange",
    "Dave Airlie",
    "David Gibson",
    "David Hildenbrand",
    "David Hoover",
    "David Marchand",
    "David du Colombier",
    "Deepak Kathayat",
    "Denis V. Lunev",
    "Dmitry Fleytman",
    "Dominik Dingel",
    "Don Slutz",
    "Dongxue Zhang",
    "Doug Kwan",
    "Dr. David Alan Gilbert",
    "Ed Maste",
    "Ed Swierk",
    "Edgar E. Iglesias",
    "Eduardo Habkost",
    "Eduardo Otubo",
    "Eric Blake",
    "Eric Farman",
    "Eugene (jno) Dvurechenski",
    "Fabian Aggeler",
    "Fabien Chouteau",
    "Fam Zheng",
    "Felix Geyer",
    "Fernando Luis V\xa0zquez Cao",
    "Frank Blaschka",
    "Frank Ch. Eigler",
    "Gabriel L. Somlo",
    "Gal Hammer",
    "Gary R Hook",
    "Gavin Shan",
    "Gerd Hoffmann",
    "Gernot Hillier",
    "Gonglei",
    "Gonglei (Arei)",
    "Grant Likely",
    "Greg Bellows",
    "Greg Kurz",
    "Gu Zheng",
    "Guenter Roeck",
    "Hani Benhabiles",
    "Hannes Reinecke",
    "Hans de Goede",
    "Heinz Graalfs",
    "Herv\x82 Poussineau",
    "Hitoshi Mitake",
    "Hu Tao",
    "Hunter Laux",
    "Huw Davies",
    "Ian Campbell",
    "Igor Mammedov",
    "Igor Ryzhov",
    "Ingo van Lil",
    "Jack Un",
    "James Harper",
    "James Hogan",
    "Jan Kiszka",
    "Jan Vesely",
    "Janne Grunau",
    "Jason J. Herne",
    "Jason Wang",
    "Jeff Cody",
    "Jens Freimann",
    "Jeremy White",
    "Jidong Xiao",
    "Jim Meyering",
    "Jincheng Miao",
    "Jiri Pirko",
    "Joakim Tjernlund",
    "Joel Schopp",
    "Joel Stanley",
    "John Snow",
    "Jonas Maebe",
    "Juan Quintela",
    "Jules Wang",
    "Jun Li",
    "Jan Tomko",
    "KONRAD Frederic",
    "Kevin O'Connor",
    "Kevin Wolf",
    "Kewei Yu",
    "Kirill A. Shutemov",
    "Kirill Batuzov",
    "Knut Omang",
    "Kusanagi Kouichi",
    "L\xa0szl\xa2 \x90rsek",
    "Laurent Dufour",
    "Le Tan",
    "Leandro Dorileo",
    "Leif Lindholm",
    "Leon Alrae",
    "Levente Kurusa",
    "Li Liu",
    "Liming Wang",
    "Liu Jinsong",
    "Liu Yuan",
    "Liu, Jinsong",
    "Liviu Ionescu",
    "Llu\xa1s Vilanova",
    "Loic Dachary",
    "Luiz Capitulino",
    "MORITA Kazutaka",
    "Maciej W. Rozycki",
    "Magnus Reftel",
    "Marc Mar\xa1",
    "Marc-Andr\x82 Lureau",
    "Marcel Apfelbaum",
    "Marcelo Tosatti",
    "Maria Kustova",
    "Mario Smarduch",
    "Mark Cave-Ayland",
    "Mark Wu",
    "Markus Armbruster",
    "Martin Decky",
    "Martin Galvan",
    "Martin Husemann",
    "Martin Kletzander",
    "Martin Simmons",
    "Matt Lupfer",
    "Matthew Booth",
    "Matthew Garrett",
    "Matthew Rosato",
    "Max Filippov",
    "Max Reitz",
    "Maxim Ostapenko",
    "Michael Buesch",
    "Michael Ellerman",
    "Michael Matz",
    "Michael Mueller",
    "Michael R. Hines",
    "Michael Roth",
    "Michael S. Tsirkin",
    "Michael Tokarev",
    "Michael Walle",
    "Michal Privoznik",
    "Mike Day",
    "Mike Frysinger",
    "Mikhail Ilyin",
    "Miki Mishael",
    "Ming Lei",
    "Miroslav Rezanina",
    "Mohamad Gebai",
    "Namhyung Kim",
    "Natanael Copa",
    "Nathan Rossi",
    "Nathan Whitehorn",
    "Nicolas Owens",
    "Nikita Belov",
    "Nikolay Nikolaev",
    "Nikunj A Dadhania",
    "Olaf Hering",
    "Olivier Danet",
    "Oran Avraham",
    "Orit Wasserman",
    "Pankaj Gupta",
    "Pantelis Koukousoulas",
    "Paolo Bonzini",
    "Paul Burton",
    "Paul Janzen",
    "Paul Moore",
    "Pavel Dovgaluk",
    "Pavel Dovgalyuk",
    "Pavel Zbitskiy",
    "Petar Jovanovic",
    "Peter Crosthwaite",
    "Peter Feiner",
    "Peter Krempa",
    "Peter Lieven",
    "Peter Maydell",
    "Peter Wu",
    "Petr Matousek",
    "Philipp Gesang",
    "Philipp Hahn",
    "Pierre Mallard",
    "Pranavkumar Sawargaonkar",
    "Prasad Joshi",
    "Qiao Nuohan",
    "Rabin Vincent",
    "Radim Krcm\xa0r",
    "Ray Strode",
    "Reza Jelveh",
    "Richard Henderson",
    "Richard W.M. Jones",
    "Rick Liu",
    "Ricky Zhou",
    "Riku Voipio",
    "Rob Herring",
    "Roger Pau Monne",
    "Roland Dreier",
    "Roy Franz",
    "Rusty Russell",
    "Sam bobroff",
    "Samuel Thibault",
    "Sangho Park",
    "Sanjay Lal",
    "Saravanakumar",
    "Sean Bruno",
    "Sebastian Huber",
    "Sebastian Krahmer",
    "Sebastian Tanase",
    "SeokYeon Hwang",
    "Sergey Fedorov",
    "Shreyas B. Prabhu",
    "Soramichi AKIYAMA",
    "Sorav Bansal",
    "Stacey Son",
    "Stanislav Vorobiov",
    "Stefan Berger",
    "Stefan Fritsch",
    "Stefan Hajnoczi",
    "Stefan Weil",
    "Stefano Stabellini",
    "Stephan Kulow",
    "Steven Noonan",
    "Stewart Smith",
    "Stratos Psomadakis",
    "Stuart Brady",
    "Takashi Iwai",
    "Thomas Falcon",
    "Thomas Huth",
    "Tim Comer",
    "Ting Wang",
    "Tom Musta",
    "Tomoki Sekiyama",
    "Tony Breeds",
    "Tristan Gingold",
    "Ulrich Obergfell",
    "Ulrich Weigand",
    "Vadim Rozenfeld",
    "Valentin Manea",
    "Vasilis Liaskovitis",
    "Vincenzo Maffione",
    "Wanlong Gao",
    "Wei Huang",
    "Wei Liu",
    "Wenchao Xia",
    "Will Newton",
    "Willem Pinckaers",
    "William Grant",
    "Wim Vander Schelden",
    "Xiaodong Gong",
    "Xin Tong",
    "Yang Zhiyong",
    "Yongbok Kim",
    "Zhang Haoyu",
    "Zhang Min",
    "Zhanghaoyu (A)",
    "Zhenzhong Duan",
    "Zhu Guihua",
    "chai wen",
    "chenfan",
    "lijun",
    "qiaonuohan",
    "thomas knych",
    "xiaoqiang zhao",
    "zhanghailiang",
};

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

struct credit_line {
    const char *name;
    int x, y, len;
};

static struct credit_line credit_lines[6] = {
    {"Mine clearing game", 80, 20},
    {"by Kevin Wolf", 80, 21},
    {"Max Reitz", 83, 22},
    {"Stefan Hajnoczi", 83, 23},
    {"QEMU contributors in 2014:", 125, 21},
    {"", 130, 23}, /* dummy line to delay start of credits */
};

static void credit_line_reset(struct credit_line *c)
{
    int i;

    c->y = 20 + rand() % 5;
    c->x = 80;

    /* Avoid overlaps and leave some space between names */
    c->name = NULL;
    for (i = 0; i < ARRAY_SIZE(credit_lines); i++) {
        struct credit_line *p = &credit_lines[i];
        if (p->name &&
            ((p->y == c->y && p->x + p->len > 70) || p->x > 77))
        {
            return;
        }
    }

    c->name = credits[rand() % ARRAY_SIZE(credits)];
    c->len = strlen(c->name);
}

static void credit_line_draw(struct credit_line *c)
{
    const char *p;
    int x;

    if (c->name == NULL) {
        return;
    }

    for (x = c->x, p = c->name; *p; x++, p++) {
        if (x < 0 || x >= 80) {
            continue;
        }
        console_setc(x, c->y, *p, 14);
    }
}

void credits_timer(void)
{
    static int timer_count;
    int i;

    /* Only update horizontal scrolling position 2/3rds of the time */
    if (timer_count++ == 2) {
        timer_count = 0;
        for (i = 0; i < sizeof(credit_lines) / sizeof(credit_lines[0]); i++) {
            credit_line_draw(&credit_lines[i]);
        }
        return;
    }

    for (i = 0; i < sizeof(credit_lines) / sizeof(credit_lines[0]); i++) {
        struct credit_line *c = &credit_lines[i];
        int len;
        const char *p;

	/* Spawn new credit lines */
        if (!c->name) {
            if (rand() % 15) {
                continue;
            }
            credit_line_reset(c);
        }

        for (p = c->name, len = 0; *p; p++) {
            len++;
        }

        c->x--;
        credit_line_draw(c);
        if (c->x + len < 80) {
            console_set(c->x + len, c->y, ' ');
        }
        if (c->x + len == 0) {
            c->name = NULL; /* disable this line */
        }
    }
}
