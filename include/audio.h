#ifndef AUDIO_H
#define AUDIO_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

bool init_audio(void (*callback)(int16_t *data, size_t samples));
void audio_play(bool play);
void audio_mute(bool mute);
void inject_samples(int16_t *data, size_t count);

#endif
