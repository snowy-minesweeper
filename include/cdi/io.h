#ifndef _CDI_IO_H_
#define _CDI_IO_H_

#include <io.h>

#define cdi_inb in8
#define cdi_inw in16
#define cdi_inl in32
#define cdi_outb out8
#define cdi_outw out16
#define cdi_outl out32

#endif
