#ifndef CONSOLE_H
#define CONSOLE_H

void console_init(void);
void clrscr(void);
void console_set_cursor(int new_x, int new_y);
unsigned char console_set_attr(unsigned char attr);
char console_get(int x, int y);
char console_getc(int x, int y, unsigned char *attr);
void console_set(int x, int y, char c);
void console_setc(int x, int y, char c, unsigned char attr);

void kputc(char c);
void kputs(const char* s);
int kprintf(const char* fmt, ...);

extern char* video;

#define panic(fmt, ...) \
    do { \
        kprintf(fmt "\n", ##__VA_ARGS__); \
        asm volatile("ud2"); \
    } while (0)

#endif
