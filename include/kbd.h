#ifndef KBD_H
#define KBD_H

#define SC_F            0x21
#define SC_M            0x32
#define SC_ENTER        0x1c

#define SC_ARROW_UP     0x148
#define SC_ARROW_DOWN   0x150
#define SC_ARROW_LEFT   0x14b
#define SC_ARROW_RIGHT  0x14d

void kbd_init(void);
void kbd_intr(void);
int kbd_wait_key(void);

#endif
