#ifndef SNOW_H
#define SNOW_H

void print_snow(void);
void move_snow(void);
void snow_set_snowfree(int x, int y, int w, int h, int value);

#endif
