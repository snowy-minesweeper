#ifndef STDBOOL_H
#define STDBOOL_H

#define bool _Bool
#define false 0
#define true  1

#endif
