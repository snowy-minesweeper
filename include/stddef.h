#ifndef STDDEF_H
#define STDDEF_H

typedef __SIZE_TYPE__ size_t;

#ifndef NULL
#define NULL ((void *)0)
#endif

#endif
