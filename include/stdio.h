#ifndef STDIO_H
#define STDIO_H

#include <multiboot.h>
#include <stddef.h>

typedef struct FILE FILE;

#ifndef SEEK_SET
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

FILE *fopen(const char *path, const char *mode);
int fclose(FILE *stream);

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);

int fseek(FILE *stream, long offset, int whence);
long ftell(FILE *stream);

#endif
