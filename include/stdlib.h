#ifndef STDLIB_H
#define STDLIB_H

#include <signal.h>
#include <stddef.h>

void *malloc(size_t size);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
void free(void *ptr);

#define alloca __builtin_alloca

int abs(int x);
long labs(long x);

void qsort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));

static inline void exit(int status)
{
    raise(SIGTERM);
}

static inline void abort(void)
{
    raise(SIGABRT);
}

#endif
