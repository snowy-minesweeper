#ifndef STRING_H
#define STRING_H

#include <stddef.h>

static inline void* memset(void* buf, int c, int n)
{
    unsigned char* p = buf;

    while (n--) {
        *p++ = c;
    }

    return buf;
}

static inline void* memcpy(void* dest, const void* src, size_t n)
{
    unsigned char* d = dest;
    const unsigned char* s = src;

    while (n--) {
        *d++ = *s++;
    }

    return dest;
}

static inline size_t strlen(const char *s)
{
    const char *start = s;

    while (*s++);

    return s - start;
}

void *memchr(const void *s, int c, size_t n);
int memcmp(const void *m1, const void *m2, size_t n);
void *memmove(void *d, const void *s, size_t n);
char *strcat(char *restrict d, const char *restrict s);
int strcmp(const char *s1, const char *s2);
char *strcpy(char *restrict d, const char *restrict s);

#endif
