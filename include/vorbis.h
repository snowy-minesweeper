#ifndef VORBIS_H
#define VORBIS_H

#include <stdbool.h>

enum sfx_type {
    SFX_WIN,
    SFX_LOSS,

    SFX_COUNT
};

bool load_music(void);
void play_sfx(enum sfx_type type);

#endif
