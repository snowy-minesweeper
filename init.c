#include "console.h"
#include "intr.h"
#include "multiboot.h"
#include "rand.h"
#include "snow.h"
#include "stddef.h"
#include "kbd.h"
#include "vorbis.h"

extern void init_title(void);
extern void load_cdi_drivers(void);
extern void minesweeper(void);

struct multiboot_module *mboot_modules;
int mboot_module_count;

void init(struct multiboot_info *mb_info)
{
    init_gdt();
    init_intr();
    init_rand();

    kbd_init();
    console_init();
    clrscr();

    mboot_modules = mb_info->mbs_mods_addr;
    mboot_module_count = mb_info->mbs_mods_count;

    init_title();

    asm volatile("sti");

    load_cdi_drivers();

    if (!load_music()) {
        panic("init: failed to initialize sound");
    }

    minesweeper();

    int kc = kbd_wait_key();
    kprintf("init: got key <%x>\n", kc);

    while (1) {
        asm volatile("cli ; hlt");
    }
}
