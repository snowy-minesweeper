#include "kbd.h"
#include "console.h"
#include "io.h"

#define KBD_DATA 0x60
#define KBD_STATUS 0x64

#define KBD_BUF_SIZE 16
static int kbd_buf[KBD_BUF_SIZE];
static int kbd_buf_head, kbd_buf_tail;

void kbd_cmd(unsigned char cmd)
{
    while (inb(KBD_STATUS) & 0x2);
    outb(KBD_DATA, cmd);

    while ((inb(KBD_STATUS) & 0x1) == 0);
    inb(KBD_DATA);
}

void kbd_init(void)
{
    while (inb(KBD_STATUS) & 0x1) {
        inb(KBD_DATA);
    }

    kbd_cmd(0xf4);
}

static void kbd_queue_key(int scancode)
{
    if ((kbd_buf_head + 1) % KBD_BUF_SIZE == kbd_buf_tail) {
        /* Buffer full, ignore keypress */
        return;
    }

    kbd_buf[kbd_buf_head] = scancode;
    kbd_buf_head = (kbd_buf_head + 1) % KBD_BUF_SIZE;
}

int kbd_wait_key(void)
{
    int ret;

    while (kbd_buf_head == kbd_buf_tail) {
        asm volatile("hlt");
    }

    ret = kbd_buf[kbd_buf_tail];
    kbd_buf_tail = (kbd_buf_tail + 1) % KBD_BUF_SIZE;
    return ret;
}

void kbd_intr(void)
{
    static enum {
        KBD_STATE_NORMAL,
        KBD_STATE_E0,
    } kbd_state = KBD_STATE_NORMAL;

    int scancode = inb(KBD_DATA);

    switch (kbd_state) {
        case KBD_STATE_NORMAL:
            if (scancode == 0xe0) {
                kbd_state = KBD_STATE_E0;
                return;
            }
            if (scancode & 0x80) {
                /* Ignore break codes */
                return;
            }
            kbd_queue_key(scancode);
            break;

        case KBD_STATE_E0:
            kbd_state = KBD_STATE_NORMAL;
            if (scancode & 0x80) {
                /* Ignore break codes */
                return;
            }
            kbd_queue_key(0x100 | scancode);
            break;
    }
}
