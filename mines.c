#include "console.h"
#include "kbd.h"
#include "snow.h"
#include "stdbool.h"
#include "rand.h"
#include "audio.h"
#include "vorbis.h"

#define MS_MAX_WIDTH 64
#define MS_MAX_HEIGHT 16

#define MS_MINE         0x1
#define MS_FLAG         0x2
#define MS_REVEALED     0x4

static int board[MS_MAX_WIDTH][MS_MAX_HEIGHT];

static int board_x;
static int board_y;
static int board_width;
static int board_height;

static int cursor_x;
static int cursor_y;

static int gameover;

bool sound_muted = false;

#define mines_message(fmt, ...) \
    do { \
        int i; \
        console_set_cursor(board_x, board_y + board_height); \
        for (i = board_x; i < board_x + board_width; i++) { \
            console_set(i, board_y + board_height, ' '); \
        } \
        console_set_cursor(board_x, board_y + board_height); \
        kprintf(fmt, ##__VA_ARGS__); \
    } while(0)

static void init_board(int width, int height)
{
    int x, y;

    snow_set_snowfree(board_x, board_y, board_width, board_height + 1, 0);

    for (x = 0; x < width; x++) {
        for (y = 0; y < height; y++) {
            board[x][y] = 0;
        }
    }

    gameover = 0;

    board_width = width;
    board_height = height;

    board_x = (80 - width) / 2;
    board_y = 2 + (18 - height) / 2;

    snow_set_snowfree(board_x, board_y, width, height + 1, 1);
}

static int count(int x, int y, bool (*counter)(int x, int y))
{
    int c = 0;

    c += counter(x - 1, y - 1);
    c += counter(x - 1, y    );
    c += counter(x - 1, y + 1);
    c += counter(x    , y - 1);
    c += counter(x    , y + 1);
    c += counter(x + 1, y - 1);
    c += counter(x + 1, y    );
    c += counter(x + 1, y + 1);

    return c;
}

static bool has_mine(int x, int y)
{
    if (x >= 0 && x < board_width &&
        y >= 0 && y < board_height)
    {
        return board[x][y] & MS_MINE;
    }

    return false;
}

static int count_adjacent_mines(int x, int y)
{
    return count(x, y, has_mine);
}

static bool is_flagged(int x, int y)
{
    if (x >= 0 && x < board_width &&
        y >= 0 && y < board_height)
    {
        return board[x][y] & MS_FLAG;
    }

    return false;
}

static int count_adjacent_flagged(int x, int y)
{
    return count(x, y, is_flagged);
}

static bool is_revealed(int x, int y)
{
    if (x >= 0 && x < board_width &&
        y >= 0 && y < board_height)
    {
        return board[x][y] & MS_REVEALED;
    }

    return false;
}

static void fill_board(int mines, int start_x, int start_y)
{
    for (int i = 0; i < mines; i++) {
        int x, y;

        do {
            x = rand() % board_width;
            y = rand() % board_height;
        } while (has_mine(x, y) || (x == start_x && y == start_y));

        board[x][y] |= MS_MINE;

        if (count_adjacent_mines(start_x, start_y)) {
            board[x][y] &= ~MS_MINE;
            i--;
            continue;
        }
    }
}

static void print_symbol(int x, int y, int value)
{
    int bg;
    int mines = count_adjacent_mines(x, y);

    if (cursor_x == x && cursor_y == y) {
        bg = gameover ? gameover == 1 ? 0xe0 : 0xe0 : 0xf0;
    } else {
        bg = gameover ? gameover == 1 ? 0x40 : 0x20 : 0x10;
    }

    x += board_x;
    y += board_y;

    if (!gameover && (value & MS_REVEALED) == 0) {
        if (value & MS_FLAG) {
            console_setc(x, y, '?', bg == 0xf0 ? 0xf1 : bg | 0xf);
        } else {
            console_setc(x, y, (x + y) & 1 ? '\xb1' : '\xb0', bg | 0x9);
        }
        return;
    }

    if (value & MS_REVEALED && bg == 0x40) {
        bg = 0xc0;
    }
    if (value & MS_MINE) {
        console_setc(x, y, '\xe9', bg | 0x0);
    } else if (mines > 0) {
        int fg = (mines % 4) + 2;
        if (fg == bg >> 4) {
            fg = 0x7;
        }
        console_setc(x, y, '0' + mines, bg | fg);
    } else if (value & MS_REVEALED) {
        console_setc(x, y, '\xdb', bg >> 4 | 0x00);
    } else {
        console_setc(x, y, '\xb2', bg >> 4 | 0x00);
    }
}

static void draw_board(void)
{
    int x, y;

    for (x = 0; x < board_width; x++) {
        for (y = 0; y < board_height; y++) {
            print_symbol(x, y, board[x][y]);
        }
    }
}

static void reveal_field(int x, int y)
{
    if (is_revealed(x, y)) {
        return;
    }

    board[x][y] |= MS_REVEALED;
    if (is_flagged(x, y)) {
        board[x][y] &= ~MS_FLAG;
    }

    if (count_adjacent_mines(x, y) == 0) {
        int i, j;

        for (i = x - 1; i <= x + 1; i++) {
            for (j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && i < board_width &&
                    j >= 0 && j < board_height)
                {
                    /* FIXME deep recursion */
                    reveal_field(i, j);
                }
            }
        }
    }
}

static bool is_unmarked(int x, int y)
{
    if (x >= 0 && x < board_width &&
        y >= 0 && y < board_height)
    {
        return !(board[x][y] & (MS_FLAG | MS_REVEALED));
    }

    return false;
}

static bool reveal_adjacent_unmarked(int x, int y)
{
    bool hit_mine = false;

    for (int nx = x - 1; nx <= x + 1; nx++) {
        for (int ny = y - 1; ny <= y + 1; ny++) {
            if (nx == x && ny == y) {
                continue;
            }

            if (is_unmarked(nx, ny)) {
                reveal_field(nx, ny);
                hit_mine |= has_mine(nx, ny);
            }
        }
    }

    return hit_mine;
}

static int missing_fields(void)
{
    int x, y;
    int count = 0;

    for (x = 0; x < board_width; x++) {
        for (y = 0; y < board_height; y++) {
            if (!is_revealed(x, y) && !has_mine(x, y)) {
                count++;
            }
        }
    }

    return count;
}

void minesweeper(void)
{
    int width = 40;
    int height = 16;
    int mines = 99;

    bool waiting_for_restart = false, first_reveal = true;

    init_board(width, height);

    while (1) {
        int kc;
        int missing;

        if (!waiting_for_restart) {
            if (first_reveal) {
                missing = width * height - mines;
            } else {
                missing = missing_fields();
            }
            if (missing == 0) {
                play_sfx(SFX_WIN);
                gameover = 2;
                draw_board();
                mines_message("Yay, game won! Press enter to restart.");
                waiting_for_restart = true;
            } else {
                console_set_cursor(board_x, board_y + board_height);
                mines_message("Move:arrows Flag:f Uncover:enter   \xb2:%d", missing);
            }
        }

        draw_board();
        kc = kbd_wait_key();
        //kprintf("<%x>\n", kc);

        switch (kc) {
            case SC_ENTER:
                if (waiting_for_restart) {
                    init_board(width, height);
                    waiting_for_restart = false;
                    first_reveal = true;
                    break;
                }
                if (first_reveal) {
                    fill_board(mines, cursor_x, cursor_y);
                    first_reveal = false;
                }
                if (is_flagged(cursor_x, cursor_y)) {
                    break;
                } else if (has_mine(cursor_x, cursor_y)) {
                    play_sfx(SFX_LOSS);
                    gameover = 1;
                    draw_board();
                    mines_message("Oops! Press enter to restart.");
                    waiting_for_restart = true;
                } else if (is_revealed(cursor_x, cursor_y)) {
                    if (count_adjacent_mines(cursor_x, cursor_y)
                        == count_adjacent_flagged(cursor_x, cursor_y))
                    {
                        if (reveal_adjacent_unmarked(cursor_x, cursor_y)) {
                            play_sfx(SFX_LOSS);
                            gameover = 1;
                            draw_board();
                            mines_message("Oops! Press enter to restart.");
                            waiting_for_restart = true;
                        }
                    }
                } else {
                    reveal_field(cursor_x, cursor_y);
                }
                break;
            case SC_F:
                if (!waiting_for_restart && !is_revealed(cursor_x, cursor_y)) {
                    board[cursor_x][cursor_y] ^= MS_FLAG;
                }
                break;
            case SC_M:
                sound_muted ^= true;
                audio_mute(sound_muted);
                break;
            case SC_ARROW_LEFT:
                if (!waiting_for_restart && cursor_x > 0) {
                    cursor_x--;
                }
                break;
            case SC_ARROW_RIGHT:
                if (!waiting_for_restart && cursor_x < board_width - 1) {
                    cursor_x++;
                }
                break;
            case SC_ARROW_UP:
                if (!waiting_for_restart && cursor_y > 0) {
                    cursor_y--;
                }
                break;
            case SC_ARROW_DOWN:
                if (!waiting_for_restart && cursor_y < board_height - 1) {
                    cursor_y++;
                }
                break;
        }
    }
}
