#include <stdint.h>
#include "rand.h"

static uint32_t seed;

void init_rand(void)
{
    asm volatile("rdtsc" : "=a" (seed));
}

unsigned int rand(void)
{
    int tsc = 0;

    asm volatile("rdtsc" : "=a" (tsc));
    seed = (1103515245 * seed) + 12345;
    seed += tsc;
    return (seed & 0x7fffffff) >> 16;
}

