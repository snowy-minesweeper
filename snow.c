#include "console.h"
#include "rand.h"

static int snowfree[80][25];

static void print_snow(void)
{
    int i;

    for (i = 0; i < 5; i++) {
        int x = rand() % 80;
        int attr = (rand() & 1) ? 7 : 15;
        console_setc(x, 0, '*', attr);
    }
}

static int next_snow_line(int x, int y)
{
    y++;
    while (y < 25 && snowfree[x][y]) {
        y++;
    }

    if (y == 25) {
        return -1;
    } else {
        return y;
    }
}

static void move_snow(void)
{
    int x, y;

    for (y = 23; y >= 0; y--) {
        for (x = 0; x < 80; x++) {
            int new_x, new_y;
            unsigned char attr;

            if (snowfree[x][y]) {
                continue;
            }
            if (console_getc(x, y, &attr) != '*' || (rand() % 3)) {
                continue;
            }

            console_set(x, y, ' ');

            new_x = (x + rand() % 3 - 1 + 80) % 80;
            new_y = next_snow_line(new_x, y);
            if (new_y < 0) {
                continue;
            }

            console_setc(new_x, new_y, '*', attr);
        }
    }

    for (x = 0; x < 80; x++) {
        if (console_get(x, 24) == '*' && !(rand() % 20)) {
            console_set(x, 24, ' ');
        }
    }
}

void snow_set_snowfree(int x, int y, int w, int h, int value)
{
    int i, j;

    if (x < 0 || w < 0 || x + w >= 80) {
        panic("register_snowfree(%d, %d, %d, %d)", x, y, w, h);
    }
    if (y < 0 || h < 0 || y + h >= 25) {
        panic("register_snowfree(%d, %d, %d, %d)", x, y, w, h);
    }

    for (i = x; i < x + w; i++) {
        for (j = y; j < y + h; j++) {
            snowfree[i][j] = value;
        }
    }
}

void snow_timer(void)
{
    move_snow();
    print_snow();
}
