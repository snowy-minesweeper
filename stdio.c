#include <console.h>
#include <multiboot.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct FILE {
    struct multiboot_module *mod;
    long loc;
};


extern struct multiboot_module *mboot_modules;
extern int mboot_module_count;


FILE *fopen(const char *path, const char *mode)
{
    for (int i = 0; i < mboot_module_count; i++) {
        if (!strcmp(path, mboot_modules[i].cmdline)) {
            FILE *fp = malloc(sizeof(*fp));
            fp->mod = &mboot_modules[i];
            fp->loc = 0;
            return fp;
        }
    }

    return NULL;
}


int fclose(FILE *stream)
{
    free(stream);
    return 0;
}


size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t byte_size = size * nmemb;
    size_t mod_len = stream->mod->mod_end - stream->mod->mod_start;

    if (stream->loc > mod_len) {
        return 0;
    }

    if (byte_size > mod_len - stream->loc) {
        byte_size = mod_len - stream->loc;
    }
    nmemb = byte_size / size;
    byte_size = nmemb * size;

    memcpy(ptr, (uint8_t *)stream->mod->mod_start + stream->loc, byte_size);
    stream->loc += byte_size;

    return nmemb;
}


long ftell(FILE *fp)
{
    return fp->loc;
}


int fseek(FILE *stream, long offset, int whence)
{
    switch (whence) {
        case SEEK_SET:
            stream->loc = offset;
            break;

        case SEEK_CUR:
            stream->loc += offset;
            break;

        case SEEK_END:
            stream->loc = stream->mod->mod_end - stream->mod->mod_start;
            stream->loc += offset;
            break;
    }

    if (stream->loc < 0) {
        stream->loc = 0;
    }

    return 0;
}
