#include <multiboot.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

extern const void kernel_end;
static uintptr_t heap_end;

extern struct multiboot_module *mboot_modules;
extern int mboot_module_count;


void *sbrk(ptrdiff_t sz)
{
    if (!heap_end) {
        heap_end = (uintptr_t)&kernel_end;
        for (int i = 0; i < mboot_module_count; i++) {
            if (mboot_modules[i].mod_end > heap_end) {
                heap_end = mboot_modules[i].mod_end;
            }
            if ((uintptr_t)(mboot_modules[i].cmdline + strlen(mboot_modules[i].cmdline) + 1) > heap_end) {
                heap_end = (uintptr_t)(mboot_modules[i].cmdline + strlen(mboot_modules[i].cmdline) + 1);
            }
        }

        if ((uintptr_t)&mboot_modules[mboot_module_count] > heap_end) {
            heap_end = (uintptr_t)&mboot_modules[mboot_module_count];
        }
    }

    void *ptr = (void *)heap_end;
    heap_end += sz;
    return ptr;
}


int abs(int x)
{
    return x < 0 ? -x : x;
}

long labs(long x)
{
    return x < 0 ? -x : x;
}


#define element(i) \
    (void *)((uintptr_t)base + i * size)

#define compare(i, j) \
    compar(element(i), element(j))

#define swap(i, j) \
    memcpy(tmp, element(i), size); \
    memcpy(element(i), element(j), size); \
    memcpy(element(j), tmp, size);

void qsort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *))
{
    uint8_t tmp[size];

    if (nmemb < 2) {
        return;
    } else if (nmemb == 2) {
        if (compare(0, 1) > 0) {
            swap(0, 1);
        }
        return;
    }

    size_t pivot = 0, i = 1, j = nmemb - 1;

    while (i < j) {
        while (i < j && compare(i, pivot) < 0) {
            i++;
        }
        while (i < j && compare(pivot, j) < 0) {
            j--;
        }
        if (i < j) {
            swap(i, j);
            i++;
            j--;
        }
    }

    if (compare(pivot, i) > 0) {
        swap(pivot, j);
    }

    qsort(element(0), i, size, compar);
    qsort(element(i), nmemb - i, size, compar);
}
