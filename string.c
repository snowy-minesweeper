#include <stdint.h>
#include <string.h>

void *memchr(const void *s, int c, size_t n)
{
    const unsigned char *b = s;

    while (n--) {
        if (*(b++) == (unsigned char)c) {
            return (void *)(b - 1);
        }
    }

    return NULL;
}

int memcmp(const void *s1, const void *s2, size_t n)
{
    int ret = 0;
    const unsigned char *b1 = s1, *b2 = s2;

    while (n-- && !(ret = *(b2++) - *(b1++)));

    return ret;
}

void *memmove(void *d, const void *s, size_t n)
{
    const uint8_t *s8 = s;
    uint8_t *d8 = d;

    if ((uintptr_t)d < (uintptr_t)s || (uintptr_t)s + n <= (uintptr_t)d) {
        while (n--) {
            *(d8++) = *(s8++);
        }
    } else {
        while (n--) {
            d8[n] = s8[n];
        }
    }

    return d;
}

char *strcat(char *restrict d, const char *restrict s)
{
    char *vd = d;

    while (*(vd++));
    vd--;

    while (*s) {
        *(vd++) = *(s++);
    }
    *vd = 0;

    return d;
}

char *strcpy(char *restrict d, const char *restrict s)
{
    char *vd = d;

    while (*s) {
        *(vd++) = *(s++);
    }
    *vd = 0;

    return d;
}

int strcmp(const char *s1, const char *s2)
{
    int ret;

    while (!(ret = (unsigned char)*(s2++) - (unsigned char)*s1)) {
        if (!*(s1++)) {
            return 0;
        }
    }

    return ret;
}
