#include "console.h"
#include "snow.h"
#include "io.h"

void init_title()
{
    unsigned char old_attr;

    snow_set_snowfree(20, 2, 40, 1, 1);

    old_attr = console_set_attr(0x8);
    console_set_cursor(20, 2);
    kprintf(".-==[  QEMU Advent  Calendar 2014  ]==-.");
    console_set_attr(old_attr);
}

void title_timer(void)
{
    static int state;
    static unsigned char red, green = 63, blue;
    unsigned char* cur;

    switch (state % 3) {
        case 0: cur = &red;     break;
        case 1: cur = &green;   break;
        case 2: cur = &blue;    break;
    }

    if (state & 1) {
        (*cur)--;
    } else {
        (*cur)++;
    }

    if (*cur == 0 || *cur == 63) {
        state++;
        state %= 6;
    }

    outb(0x3c8, 56);
    outb(0x3c9, red);
    outb(0x3c9, green);
    outb(0x3c9, blue);
}
