#include <audio.h>
#include <console.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <vorbis.h>
#include <ivorbisfile.h>


static void decode(int16_t *buffer, size_t samples);

static OggVorbis_File vorbis_file;
static int bitstream;


struct sfx {
    const char *name;

    void *data;
    size_t samples;
};

struct sfx sfx_list[SFX_COUNT] = {
    [SFX_WIN]  = { .name = "win.ogg"  },
    [SFX_LOSS] = { .name = "loss.ogg" },
};


static bool load_sfx(struct sfx *sfx)
{
    FILE *fp = fopen(sfx->name, "rb");
    if (!fp) {
        return false;
    }

    OggVorbis_File ovf;
    if (ov_open(fp, &ovf, NULL, 0) < 0) {
        fclose(fp);
        return false;
    }

    int64_t sz = ov_pcm_total(&ovf, -1);
    if (sz < 0) {
        ov_clear(&ovf);
        return false;
    }

    /* apparently ov_pcm_total() does not return the number of samples, but
     * the number of frames */
    sz *= 2;

    void *buf = malloc(sz * 2), *ptr = buf;
    int bs;

    sfx->data = buf;
    sfx->samples = sz;

    while (sz) {
        int ret = ov_read(&ovf, ptr, sz * 2, &bs);
        if (ret <= 0) {
            break;
        }

        sz -= ret / 2;
        ptr = (void *)((uintptr_t)ptr + ret);
    }

    ov_clear(&ovf);
    return true;
}


bool load_music(void)
{
    if (!init_audio(decode)) {
        return false;
    }

    for (size_t i = 0; i < sizeof(sfx_list) / sizeof(sfx_list[0]); i++) {
        if (!load_sfx(&sfx_list[i])) {
            panic("Failed to find %s", sfx_list[i].name);
        }
    }

    FILE *fp = fopen("music.ogg", "rb");
    if (!fp) {
        panic("Failed to find music.ogg");
    }

    if (ov_open(fp, &vorbis_file, NULL, 0) < 0) {
        fclose(fp);
        return false;
    }

    audio_play(true);

    return true;
}


void play_sfx(enum sfx_type type)
{
    inject_samples(sfx_list[type].data, sfx_list[type].samples);
}


static void decode(int16_t *buffer, size_t samples)
{
    size_t remaining = samples;

    do {
        int now = ov_read(&vorbis_file, (void *)buffer, remaining * 2, &bitstream);

        if (now <= 0) {
            now = 0;
            ov_raw_seek(&vorbis_file, 0);
        }

        remaining -= now / 2;
        buffer = buffer + now / 2;
    } while (remaining);
}
